<?php if ($is_front): ?>




<b><a href="<?php print $node_url ?>" title="<?php print $title ?>"><?php print $title ?></a></b>
<img src="<?php print $node->picture;?>" style="margin: 0px 0px 0px 0px; float: none; border: 1px solid #AA2BB4;"  width=270px height=150px alt=<?php print $title ?>/>
<p> <?php print $content ?>...&nbsp;<a href="<?php print $node_url ?>" title="<?php print $title ?>"></a></p>				
<div style="border-bottom: 1px dotted rgb(192, 192, 192); padding: 0px 0px 10px; margin-bottom: 10px; clear: both;"></div>
<?php else: ?>



<div id="node-<?php print $node->nid; ?>" class="node<?php if ($sticky) { print ' sticky'; } ?><?php if (!$status) { print ' node-unpublished'; } ?>">

  
  <?php print $picture ?>

  <?php if ($page == 0): ?>
    <h2><a href="<?php print $node_url ?>" title="<?php print $title ?>"><?php print $title ?></a></h2>
  <?php endif; ?>

  <?php if ($submitted): ?>
    <span class="submitted"><?php print $submitted; ?></span>
  <?php endif; ?>

  <div class="content">
    <?php print $content ?>
  </div>

  <?php if ($links||$taxonomy){ ?>
    <div class="meta">

      <?php if ($links): ?>
        <div class="links">
          <?php print $links; ?>
        </div>
      <?php endif; ?>

      <?php if ($taxonomy): ?>
        <div class="terms">
          <?php print $terms ?>
        </div>
      <?php endif;?>

      <span class="clear"></span>

    </div>
  <?php }?>

</div>
<?php endif; ?>

