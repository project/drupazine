<!DOCTYPE HTML PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"><head profile="http://gmpg.org/xfn/11">


<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta name="distribution" content="global">
<meta name="robots" content="follow, all">
<meta name="language" content="en, sv">

<title><?php print $head_title ?></title>

<?php print $head ?>
<?php print $styles ?>
<?php print $scripts ?>

<style type="text/css">
/*Credits: CSSpplay */
/*URL: http://www.cssplay.co.uk/menus/pro_one */

.menu1 {padding:0 0 0 32px; margin:0; list-style:none; height:35px; background:#fff url(themes/drupazine/images/button1.gif); position:relative; border:1px solid #000; border-width:0 1px; border-bottom:1px solid #444;}
.menu1 li {float:left;}
.menu1 li a {display:block; float:left; height:35px; line-height:35px; color:#aaa; text-decoration:none; font-size:11px; font-family:arial, verdana, sans-serif; font-weight:bold; text-align:center; padding:0 0 0 8px; cursor:pointer;}
.menu1 li a b {float:left; display:block; padding:0 16px 0 8px;}
.menu1 li.current a {color:#fff; background:url(themes/drupazine/images/button3.gif);}
.menu1 li.current a b {background:url(themes/drupazine/images/button3.gif) no-repeat right top;}
.menu1 li a:hover {color:#fff; background:#000 url(themes/drupazine/images/button2.gif);}
.menu1 li a:hover b {background:url(themes/drupazine/images/button2.gif) no-repeat right top;}
.menu1 li.current a:hover {color:#fff; background:#000 url(themes/drupazine/images/button3.gif); cursor:default;}
.menu1 li.current a:hover b {background:url(themes/drupazine/images/button3.gif) no-repeat right top;}

</style>

<script type="text/javascript"><!--//--><![CDATA[//><!--
sfHover = function() {
	if (!document.getElementsByTagName) return false;
	var sfEls = document.getElementById("nav").getElementsByTagName("li");

	// if you only have one main menu - delete the line below //
	var sfEls1 = document.getElementById("subnav").getElementsByTagName("li");
	//

	for (var i=0; i<sfEls.length; i++) {
		sfEls[i].onmouseover=function() {
			this.className+=" sfhover";
		}
		sfEls[i].onmouseout=function() {
			this.className=this.className.replace(new RegExp(" sfhover\\b"), "");
		}
	}

	// if you only have one main menu - delete the "for" loop below //
	for (var i=0; i<sfEls1.length; i++) {
		sfEls1[i].onmouseover=function() {
			this.className+=" sfhover1";
		}
		sfEls1[i].onmouseout=function() {
			this.className=this.className.replace(new RegExp(" sfhover1\\b"), "");
		}
	}
	//

}
if (window.attachEvent) window.attachEvent("onload", sfHover);
//--><!]]></script>

</head><body>




<div id="wrap">




<div id="header">
	<ul class="menu1">
		<?php print theme('links', $primary_links, array('class' => 'links primary-links')) ?>
	</ul>
	<?php print $header; ?>
	<div class="headerleft">
	
		<?php print '<a href="'. check_url($front_page) .'" title="'. $site_name .'">';
          if ($logo) {
            print '<img src="'. check_url($logo) .'" alt="'. $site_name .'" id="logo" />';
          }
          
          print '</a>';
		?>     
	</div>
	
	

	
	<div class="headerright">
		<?php print $headerright; ?>
	</div>

</div>



<div id="subnavbar">

	<ul id="subnav">
		<?php print theme('links', $secondary_links, array('class' => 'links secondary-links')) ?>
	</ul>
	
</div>
<div id="content">
	
	<?php if($is_front): ?>
	<div id="homepage">
		<div id="homepageleft">
			<?php print $left ?>
		</div>

		<div id="homepageright">
			<div class="thumb">
			<?php if ($mission): print '<div id="mission">'. $mission .'</div><div style="border-bottom: 1px dotted rgb(192, 192, 192); padding: 0px 0px 10px; margin-bottom: 10px; clear: both;"></div>'; endif; ?>
			<?php if ($tabs): print '<div id="tabs-wrapper" class="clear-block">'; endif; ?>
			<?php if ($title): print '<h2'. ($tabs ? ' class="with-tabs"' : '') .'>'. $title .'</h2>'; endif; ?>
			<?php if ($tabs): print '<ul class="tabs primary">'. $tabs .'</ul></div>'; endif; ?>
			<?php if ($tabs2): print '<ul class="tabs secondary">'. $tabs2 .'</ul>'; endif; ?>
			<?php if ($tabs): print '<div style="border-bottom: 1px dotted rgb(192, 192, 192); padding: 0px 0px 10px; margin-bottom: 10px; clear: both;"></div>'; endif; ?>
			<?php if ($show_messages && $messages): print $messages; endif; ?>
			<?php print $help; ?>
			<?php print $content ?>
			</div>
		</div>
		
	</div>
	<?php else:?>
		<div id="contentleft">
	
			<div class="postarea">
		
				
					<?php print $breadcrumb; ?>
						
				<?php if ($mission): print '<div id="mission">'. $mission .'</div><div style="border-bottom: 1px dotted rgb(192, 192, 192); padding: 0px 0px 10px; margin-bottom: 10px; clear: both;"></div>'; endif; ?>
				<?php if ($tabs): print '<div id="tabs-wrapper" class="clear-block">'; endif; ?>
				<?php if ($title): print '<h2'. ($tabs ? ' class="with-tabs"' : '') .'>'. $title .'</h2>'; endif; ?>
				<?php if ($tabs): print '<ul class="tabs primary">'. $tabs .'</ul></div>'; endif; ?>
				<?php if ($tabs2): print '<ul class="tabs secondary">'. $tabs2 .'</ul>'; endif; ?>
				<?php if ($tabs): print '<div style="border-bottom: 1px dotted rgb(192, 192, 192); padding: 0px 0px 10px; margin-bottom: 10px; clear: both;"></div>'; endif; ?>
				<?php if ($show_messages && $messages): print $messages; endif; ?>
				<?php print $help; ?>
				<?php print $content ?>
			</div>
		
			
		
		</div>

    <?php endif; ?>	
<!-- begin sidebar -->

<div id="sidebar">
	<?php print $right ?>
</div>

<!-- end sidebar -->		
</div>

<!-- The main column ends  -->

<!-- begin footer -->

<div style="clear: both;"></div>
<center>
<!-- do not remove this link,please-->
Drupazine is a Free Premium Drupal Magazine Theme  by <a href=http://www.coracaodejesusedemaria.com>FRATERNIDADE</a>
<!--thanks-->
</center>
<div id="footer">

	<div id="footerleft">
		<?php print $footer_message . $footer ?>
	</div>

	
	
	
</div>


<div id="bottom">
	<img src="themes/drupazine/images/bottom.gif" alt="Bottom">
</div>
<div id="footerright">
	    
		<?php print $feed_icons ?>
	</div>
</div>

</body></html>